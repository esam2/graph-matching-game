//! This is a minimal implementation of a frontend for interacting with Interactable objects

use frac::my::interactable::*;
use frac::my::sin::Sin;
use frac::my::tree::Tree;
use std::io;
use std::process::Command;

/// Its the main function
fn main() {
    frac_select_loop();
}

/// a loop for selecting  which item to interact with
fn frac_select_loop() {
    fn print_frac_menu() {
        println!("Main Menu:");
        println!("t: Tree");
        println!("s: Sin");
        println!("enter selection or 'q' to quit: ");
    }
    print_frac_menu();
    while let Some(k) = get_input() {
        match k {
            't' => attr_select_loop(&mut Tree::new()),
            's' => attr_select_loop(&mut Sin::new()),
            _ => println!("item not found"),
        };
        print_frac_menu();
    }
}

/// a loop for selecting  attribute to modify
fn attr_select_loop<T: Interactable + std::fmt::Display>(item: &mut T) {
    fn print_atr_select_menu<T: Interactable + std::fmt::Display>(item: &T) {
        println!("{item}");
        println!("enter number of attribute to modify or q to quit: ");
    }

    print_atr_select_menu(item);
    while let Some(k) = get_input() {
        let result = String::from(k).parse::<usize>();
        //process result
        match result {
            Ok(num) => inc_dec_loop(item, num),
            Err(_) => println!("invalid input"),
        }
        print_atr_select_menu(item);
    }
}

/// a loop for incrementing and decrementing the selected attribute
fn inc_dec_loop<T: Interactable + std::fmt::Display>(item: &mut T, num: usize) {
    fn print_inc_dec_loop_menu<T: Interactable + std::fmt::Display>(item: &T, num: usize) {
        println!("{item}");
        println!("modifying attribute: {num}");
        println!("enter 'i' to increment, 'd' to decrement, or 'q' to quit:");
    }
    print_inc_dec_loop_menu(item, num);
    while let Some(k) = get_input() {
        //process result
        let result = match k {
            'i' => item.inc_atr(num),
            'd' => item.dec_atr(num),
            _ => {
                println!("invalid input");
                Ok(())
            }
        };
        
        match result {
            Ok(_) => item.render().expect("unable to render interactable item"),
            Err(InteractionError::AtMax) => println!("Attribute already at maximum value"),
            Err(InteractionError::AtMin) => println!("Attribute already at minimum value"),
            Err(InteractionError::AtrNotFound) => println!("attribute number not present"),
        }
        print_inc_dec_loop_menu(item, num);
    }
}

/// gets a char input or returns None for input 'q'
fn get_input() -> Option<char> {
    loop {
        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("Error getting input");
        println!("input: {}", input);
        match input.trim().parse::<char>() {
            Ok(c) => {
                if c == 'q' {
                    break None;
                }
                break Some(c);
            }
            Err(_) => {
                println!("input must be single character");
            }
        }
    }
}
