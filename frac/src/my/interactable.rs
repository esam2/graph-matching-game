use std::ops::*;
use thiserror::Error;

/// conveys why an InteractableAttribute could not be modified
#[derive(Error, Debug)]
#[error("InteractionError")]
pub enum InteractionError {
    AtrNotFound,
    AtMax,
    AtMin,
}
/// defines an object that can be modifed by the user and rendered
pub trait Interactable {
    fn new() -> Self;
    fn inc_atr(&mut self, atr_num: usize) -> Result<(), InteractionError>;
    fn dec_atr(&mut self, atr_num: usize) -> Result<(), InteractionError>;
    fn render(&self) -> Result<(), Box<dyn std::error::Error>>;
}

#[derive(Copy, Clone, Debug)]
/// An attribute that can be modified by the user, either increased or decreased within specified limits
/// this internal value is interpolated into either ints or floats before it is used in the display or render functions
pub struct InteractableAttribute<T> {
    current: i16,
    max: i16,
    interp_min: T,
    interp_max: T,
}

impl<T> InteractableAttribute<T>
where
    T: std::fmt::Display
        + Copy
        + PartialOrd
        + Div
        + From<<T as Div>::Output>
        + Mul
        + From<<T as Mul>::Output>
        + AddAssign
        + SubAssign
        + Add
        + From<<T as Add>::Output>
        + Sub
        + From<i16>
        + From<<T as Sub>::Output>,
{
    /// creates a new InteractableAttribute value. Internal values are i16 from 0 - max with a default value of current.
    /// These internal values are translated to output values by the interp() method
    pub fn new(current: i16, max: i16, interp_min: T, interp_max: T) -> Self {
        InteractableAttribute {
            current,
            max,
            interp_min,
            interp_max,
        }
    }
    /// increment the internal value
    /// returns InteractionError::AtMax if already at max value
    pub fn inc(&mut self) -> Result<(), InteractionError> {
        if self.current < self.max {
            self.current += 1;
            Ok(())
        } else {
            Err(InteractionError::AtMax)
        }
    }

    /// decrement the internal value
    /// returns InteractionError::AtMax if already at min value
    pub fn dec(&mut self) -> Result<(), InteractionError> {
        if self.current > 0 {
            self.current -= 1;
            Ok(())
        } else {
            Err(InteractionError::AtMin)
        }
    }

    /*this function takes the values of current:i16 and max:16
    and outputs type t after inpolating it against the range from interp_min -> intermax
    it could be an in or a float depending on the type T
    I wanted it to do arithmetic based on the type so I did a lot of casting to the type using 'T::from'
    this imposed a ton of constraints on the generic which are of course satisfied by the types
    I actually want to use this function for but it is pretty verbose and I'm not sure if there is a cleaner way to do this*/
    //I'm doing this interpolation because I want the internal representation of the aspects to be ints and
    // therefore not have rounding errors when modifying values
    //I found the rounding errors were evident as the recursive calls magnify even small rounding errors

    /// Interpolates from internal i16 attribute values to output values of either int or float types
    pub fn interpolate(&self) -> T {
        let spread = T::from(self.interp_max - self.interp_min);
        let a = T::from(T::from(self.current) * spread);
        let b = T::from(a / T::from(self.max));
        T::from(b + self.interp_min)
    }
}
