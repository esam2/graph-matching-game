use crate::my::interactable::*;
use plotters::prelude::*;


/// A sin wave that can be modified in period, amplitude, and absolute value
#[derive(Copy, Clone, Debug)]
pub struct Sin {
    period: InteractableAttribute<f32>,
    amplitude: InteractableAttribute<f32>,
    abs: InteractableAttribute<i32>,
}

impl Interactable for Sin {
    fn new() -> Self {
        Sin {
            period: InteractableAttribute::new(4, 9, 0.5, 1.5),
            amplitude: InteractableAttribute::new(4, 9, 0.5, 2.0),
            abs: InteractableAttribute::new(0, 1, 0, 1),
        }
    }

    fn inc_atr(&mut self, atr_num: usize) -> Result<(), InteractionError> {
        match atr_num {
            1 => self.period.inc(),
            2 => self.amplitude.inc(),
            3 => self.abs.inc(),
            _ => Err(InteractionError::AtrNotFound),
        }
    }

    fn dec_atr(&mut self, atr_num: usize) -> Result<(), InteractionError> {
        match atr_num {
            1 => self.period.dec(),
            2 => self.amplitude.dec(),
            3 => self.abs.dec(),
            _ => Err(InteractionError::AtrNotFound),
        }
    }

    fn render(&self) -> Result<(), Box<dyn std::error::Error>> {
        const OUT_FILE_NAME: &str = "output.png";
        let root = BitMapBackend::new(OUT_FILE_NAME, (800, 600)).into_drawing_area();
        const XSTEP: i32 = 1000;
        const XSTEPF: f32 = XSTEP as f32;

        root.fill(&WHITE)?;
        let mut chart = ChartBuilder::on(&root)
            .build_cartesian_2d(-10f32..10f32, -10f32..10f32)
            ?;

        let range = -10 * XSTEP..=10 * XSTEP;
        let sin = range.map(|x| {
            (
                x as f32 / XSTEPF,
                (x as f32 * self.period.interpolate() / XSTEPF).sin(),
            )
        });
        let amp = sin.map(|(x, y)| (x, y * self.amplitude.interpolate()));
        let abs: Vec<(f32, f32)> = match self.abs.interpolate() {
            1 => amp.map(|(x, y)| (x, y.abs())).collect(),
            _ => amp.collect(),
        };

        chart.draw_series(LineSeries::new(abs, &BLUE))?;

        // To avoid the IO failure being ignored silently, we manually call the present function
        root.present().expect("Unable to write result to file, please make sure 'plotters-doc-data' dir exists under current dir");
        println!("Result has been saved to {}", OUT_FILE_NAME);
        Ok(())
    }
}

impl std::fmt::Display for Sin {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "1. period: {}\n2. amplitude: {}\n3. abs: {}",
            self.period.interpolate(),
            self.amplitude.interpolate(),
            self.abs.interpolate()
        )
    }
}
