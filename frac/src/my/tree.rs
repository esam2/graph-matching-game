use crate::my::interactable::*;
use plotters::prelude::*;
use std::f32::consts::*;

type Line = ((f32, f32), (f32, f32));

#[derive(Copy, Clone, Debug)]
pub struct Tree {
    distance: InteractableAttribute<f32>,
    num_branches: InteractableAttribute<i32>,
    angle: InteractableAttribute<f32>,
    distance_falloff: InteractableAttribute<f32>,
    generations: InteractableAttribute<i32>,
}

impl Interactable for Tree {
    fn new() -> Tree {
        Tree {
            distance: InteractableAttribute::<f32>::new(5, 9, 0.5, 5.0),
            num_branches: InteractableAttribute::<i32>::new(1, 5, 1, 6),
            angle: InteractableAttribute::<f32>::new(12, 24, 0.0, 2.0 * PI),
            distance_falloff: InteractableAttribute::<f32>::new(0, 9, 0.5, 1.5),
            generations: InteractableAttribute::<i32>::new(4, 10, 0, 10),
        }
    }

    fn inc_atr(&mut self, atr_num: usize) -> Result<(), InteractionError> {
        match atr_num {
            1 => self.distance.inc(),
            2 => self.num_branches.inc(),
            3 => self.angle.inc(),
            4 => self.distance_falloff.inc(),
            5 => self.generations.inc(),
            _ => Err(InteractionError::AtrNotFound),
        }
    }

    fn dec_atr(&mut self, atr_num: usize) -> Result<(), InteractionError> {
        match atr_num {
            1 => self.distance.dec(),
            2 => self.num_branches.dec(),
            3 => self.angle.dec(),
            4 => self.distance_falloff.dec(),
            5 => self.generations.dec(),
            _ => Err(InteractionError::AtrNotFound),
        }
    }

    fn render(&self) -> Result<(), Box<dyn std::error::Error>> {
        const OUT_FILE_NAME: &str = "output.png";
        let root = BitMapBackend::new(OUT_FILE_NAME, (500, 500)).into_drawing_area();
        root.fill(&WHITE)?;
        let mut chart =
            ChartBuilder::on(&root).build_cartesian_2d(-10.0..10.0_f32, -10.0..10.0_f32)?;

        let mut lines: Vec<((f32, f32), (f32, f32))> = vec![];
        self.build_recursive(
            &mut lines,
            self.generations.interpolate(),
            (0.0, 0.0),
            0.0,
            self.distance.interpolate(),
        );

        for line in lines {
            chart.draw_series(std::iter::once(PathElement::new(
                vec![line.0, line.1],
                ShapeStyle {
                    color: RED.to_rgba(),
                    filled: true,
                    stroke_width: 2,
                },
            )))?;
        }
        // To avoid the IO failure being ignored silently, we manually call the present function
        root.present().expect("Unable to write result to file");
        println!("Result has been saved to output.png");
        Ok(())
    }
}

impl std::fmt::Display for Tree {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        write!(f, "1. distance: {}\n2. num_branches: {}\n3. angle: {}\n4. distance_falloff: {}\n5. generations: {}", self.distance.interpolate(), self.num_branches.interpolate(), self.angle.interpolate(), self.distance_falloff.interpolate(), self.generations.interpolate())
    }
}

impl Tree {
    fn build_recursive(
        self,
        lines: &mut Vec<Line>,
        gens_left: i32,
        start: (f32, f32),
        current_angle: f32,
        current_distance: f32,
    ) {
        if gens_left > 0 {
            for branch_no in 0..self.num_branches.interpolate() {
                //calculate angle
                let angle: f32 = Tree::calculate_angle(
                    current_angle,
                    self.angle.interpolate(),
                    branch_no,
                    self.num_branches.interpolate(),
                );
                //calculate current line
                let end: (f32, f32) = Tree::calculate_end(start, angle, current_distance);
                //add to self.Lines
                lines.push((start, end));
                //call self
                self.build_recursive(
                    lines,
                    gens_left - 1,
                    end,
                    angle,
                    current_distance * self.distance_falloff.interpolate(),
                );
            }
        }
    }

    fn calculate_angle(
        current_angle: f32,
        spread_angle: f32,
        branch_no: i32,
        num_branches: i32,
    ) -> f32 {
        let start = current_angle - (spread_angle / 2.0);
        let step = spread_angle / (num_branches - 1) as f32;
        start + (step * branch_no as f32) % (2.0 * PI)
    }

    fn calculate_end(start: (f32, f32), angle: f32, distance: f32) -> (f32, f32) {
        (
            start.0 + (angle.cos() * distance),
            start.1 + (angle.sin() * distance),
        )
    }
}
