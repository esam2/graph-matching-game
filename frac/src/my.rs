/// provides interface defining an interactable object
pub mod interactable;
/// implementation of the Sin wave interactable object
pub mod sin;
/// implementation of the fractal tree interactable object
pub mod tree;
