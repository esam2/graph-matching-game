# Graph Matching Game
Copyright &copy; 2022 Ethan Samuels-Ellingson

This project provides an interface for interactable visualizations that can be modified by inputs from the user.


in the frontend directory build with cargo and run. Output is store in frontend/output.png

This is a work in progress:
* "Game" aspects are on pause. It is currently more of a press buttons and see what happens type interaction
* The focus was primarily on getting familiar with Rust and trying to develop an abstraction that could be applied to more interactable objects in the future
* The current methods for capturing input and displaying output are clumsy and are intended to be swapped out with a better frontend in the future. I sunk a good amount of time into trying to use OpenGl to no avail

This program is licensed under the "MIT License".  Please
see the file LICENSE in the source distribution of this
software for license terms.